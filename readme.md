# Galerie Chaltin

Bienvenue sur le site de la galerie Chaltin. Voici un tour d'horizon des fonctionnalités du site.

## Installation

Tout d'abord pour l'installation, il suffit de récupérer le projet et d'entrer la commande "composer install".

Autre info à savoir, vous pouvez vous connecter au site en tant qu'admin avec "patmar@gmail.com" et avec le mot de passe "password".

## Partie visiteur

En tant que visiteur, vous pouvez simplement consulter le site. Vous pouvez visualiser les peintures sur la page d'accueil et les observer plus en détail sur leur page dédiée.

## Partie Utilisateur

Les utilisateurs enregistrés peuvent mettre des peintures en favoris ainsi que dans le panier. Ils peuvent voir les favoris sur leur page de profil.

## Partie Admin

L'admin à accès à l'administration. De là, il peut ajouter, modifier ou supprimer des peintures. C'est là également qu'il peut choisir quelles œuvres sont dans le slider de la page d'accueil (limité à 10 images max).
