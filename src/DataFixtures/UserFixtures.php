<?php

namespace App\DataFixtures;

use App\Entity\User;
use Cocur\Slugify\Slugify;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Faker;

class UserFixtures extends Fixture
{
    private object $hasher;
    private array $genders = ['male', 'female'];

    public function __construct(UserPasswordHasherInterface $hasher){
        $this->hasher = $hasher;
    }
    public function load(ObjectManager $manager): void
    {
        $faker = Faker\Factory::create();
        $roles = ['ROLE_USER', 'ROLE_TEACHER'];
        for ($i = 1; $i <= 50; $i++){
            $user = new User();
            $slug = new Slugify();
            $gender = $faker->randomElement($this->genders);
            $user->setFirstName($faker->firstName($gender))
                ->setLastName($faker->lastName)
                ->setEmail($slug->slugify($user->getFirstName()).'.'.
                    $slug->slugify($user->getLastName()).'@'.$faker->freeEmailDomain());

            $gender = $gender == 'male' ? 'm' : 'f';
            $user->setImageName('0'. ($i+9).$gender.'.jpg')
                ->setPassword($this->hasher->hashPassword($user, 'password'))
                ->setCreatedAt(new \DateTimeImmutable())
                ->setUpdatedAt(new \DateTimeImmutable())
                ->setIsDisabled($faker->boolean(10))
            ;
            if ($faker->boolean(30) == true){
                $user->setRoles(['ROLE_SELLER']);
            }else{
                $user->setRoles(['ROLE_USER']);
            }
            $manager->persist($user);
        }

        //admin John Doe
        $user = new User();
        $user->setFirstName('John')
            ->setLastName('Doe')
            ->setEmail('john.doe@gmail.com')
            ->setImageName('022m.jpg')
            ->setPassword($this->hasher->hashPassword($user, 'password'))
            ->setCreatedAt(new \DateTimeImmutable())
            ->setUpdatedAt(new \DateTimeImmutable())
            ->setIsDisabled(false)
            ->setRoles(['ROLE_ADMIN']);
        $manager->persist($user);

        //admin Pat Mar
        $user = new User();
        $user->setFirstName('Pat')
            ->setLastName('Mar')
            ->setEmail('patmar@gmail.com')
            ->setImageName('065m.jpg')
            ->setPassword($this->hasher->hashPassword($user, 'password'))
            ->setCreatedAt(new \DateTimeImmutable())
            ->setUpdatedAt(new \DateTimeImmutable())
            ->setIsDisabled(false)
            ->setRoles(['ROLE_SUPER_ADMIN']);
        $manager->persist($user);

        //SUPER ADMIN Cha Flo
        $user = new User();
        $user->setFirstName('Flo')
            ->setLastName('Cha')
            ->setEmail('flo.cha@gmail.com')
            ->setImageName('029m.jpg')
            ->setPassword($this->hasher->hashPassword($user, 'password'))
            ->setCreatedAt(new \DateTimeImmutable())
            ->setUpdatedAt(new \DateTimeImmutable())
            ->setIsDisabled(false)
            ->setRoles(['ROLE_SUPER_ADMIN']);
        $manager->persist($user);

        $manager->flush();
    }
}
