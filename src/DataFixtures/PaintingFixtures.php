<?php

namespace App\DataFixtures;

use App\Entity\Author;
use App\Entity\Category;
use App\Entity\Painting;
use App\Entity\Technical;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker;

class PaintingFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $faker = Faker\Factory::create();
        $technicals = $manager->getRepository(Technical::class)->findAll();
        $categories = $manager->getRepository(Category::class)->findAll();
        $author = $manager->getRepository(Author::class)->findAll();

        $countCat = count($categories);
        $countTech = count($technicals);
        $countAuth = count($author);
        $date = new \DateTimeImmutable();


        for ($i=1; $i <=20; $i++) {
            $d = $faker->numberBetween(100,500);
            $creation = $date->sub(new \DateInterval('P'.$d.'Y'));

            $paint = new Painting();
            $paint->setTitle($faker->words($faker->numberBetween(3,5), true))
                ->setDescription($faker->paragraphs(3, true))
                ->setCreatedAt($creation)
                ->setAddedAt(new \DateTimeImmutable())
                ->setHeight($faker->numberBetween(20, 200))
                ->setWidth($faker->numberBetween(20, 200))
                ->setImage($i.".jpg")
                ->setCategory($categories[$faker->numberBetween(0, $countCat-1)])
                ->setTechnical($technicals[$faker->numberBetween(0, $countTech-1)])
                ->setAuthor($author[$faker->numberBetween(0, $countAuth-1)])
                ->setPrice($faker->numberBetween(20,100 ))
            ;
            if ($i <=5){
                $paint->setIsSlider(true);
            }else{
                $paint->setIsSlider(false);
            }

            $manager->persist($paint);
        }


        $manager->flush();
    }

    public function getDependencies(){

        return[
            TechnicalFixtures::class,
            CategoryFixtures::class,
            AuthorFixtures::class,
        ];
    }
}
