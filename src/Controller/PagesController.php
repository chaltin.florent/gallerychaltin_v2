<?php

namespace App\Controller;

use App\Entity\Author;
use App\Entity\Comment;
use App\Entity\Painting;
use App\Entity\User;
use App\Form\CommentType;
use App\Repository\AuthorRepository;
use App\Repository\CommentRepository;
use App\Repository\PaintingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PagesController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function home(PaintingRepository $paintingRepository): Response
    {
        $paintings = $paintingRepository->findby(
            [],
            ['addedAt' => 'DESC'],
        );
        return $this->render('pages/home.html.twig', [
            'paintings' =>  $paintings,
        ]);
    }

    #[Route('/about', name: 'about')]
    public function about(): Response
    {
        return $this->render('pages/about.html.twig');
    }

    #[Route('/team', name: 'team')]
    public function team(): Response
    {
        return $this->render('pages/team.html.twig');
    }

    #[Route('/paint/{id}', name: 'paint')]
    public function paint(Painting $paintings, EntityManagerInterface $manager, Request $request, PaintingRepository $paintingRepository, CommentRepository $commentRepository): Response
    {

        $user = $this->getUser();

        $routeParams = $request->attributes->get('_route_params');
        $painting = $paintingRepository->find($routeParams['id']);
        $comrep = $commentRepository->findby(
            ['painting' => $routeParams['id'], 'isPublished' => true],
            ['createdAt' => 'DESC']
        );

        $com = new Comment;
        $form = $this->createForm(CommentType::class, $com);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $com->setName($user->getFirstName() ." ". $user->getLastName())
                ->setCreatedAt(new \DateTimeImmutable())
                ->setPainting($painting);
            $com->setIsPublished(true);
            $manager->persist($com);
            $manager->flush();
            return $this->redirect($routeParams['id']);
        }
        return $this->renderForm('pages/paint.html.twig', [
            'form' => $form,
            'paint' => $paintings,
            'coms' => $comrep

        ]);

//        return $this->render('pages/paint.html.twig', [
//            'paint' => $painting,
//        ]);
    }


    #[Route('/author/{id}', name: 'app_author')]
    public function author(Author $author, AuthorRepository $authorRepository, Request $request): Response
    {
        $artworks = $author->getArtwork();
        $authors = $authorRepository->findAll();

        return $this->render('pages/author.html.twig', [
            'author' => $author,
            'artworks' => $artworks,
            'authors' => $authors
        ]);
    }

}
