<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use phpDocumentor\Reflection\Types\This;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    /**
     * @param MailerInterface $mailer
     * @return Response
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    #[Route('/contact/test', name: 'app_contact_test')]
    public function testEmail(MailerInterface $mailer): Response
    {
        $email = new Email();
        $email->from('pat@gmail.com')
            ->to('admin@iepscf.be')
            ->cc('john@gmail.com')
            ->subject('Demande de distanciel')
//            ->text('Je vous accorde le distanciel avec plaisir.')
            ->html("<h2>Suite a votre demande</h2><h3>Autorisation</h3><p>Fournir les dates</p>")
        ;
        $mailer->send($email);
        return $this->redirectToRoute('home');
    }

    #[Route('/contact', name: 'app_contact')]
    public function sendEmail(MailerInterface $mailer, Request $request): Response
    {
        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $email = new Email();
            $email->from($contact->getEmail())
                ->to('info@iepscf.be')
                ->subject($contact->getSubject())
                //->text($contact->getMessage())
                ->html("<p>Nouveau message de: ".$contact->getFirstName(). " ". $contact->getLastName(). "</p>
                            <p>".$contact->getMessage()."</p>")
            ;
            $mailer->send($email);
            $this->addFlash(
                'success',
                'Votre mail a bien été envoyé'
            );
            return $this->redirectToRoute('home');
        }

        return $this->renderForm('contact/contact.html.twig', [
            'form' => $form,
        ]);

    }


    #[Route('/contact/template', name: 'app_contact_template')]
    public function sendEmailTemplate(MailerInterface $mailer, Request $request): Response
    {
        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $email = new TemplatedEmail();
            $email->from($contact->getEmail())
                ->to('info@iepscf.be')
                ->subject($contact->getSubject())
                //->text($contact->getMessage())
                ->htmlTemplate('contact/email-css.html.twig')
                ->context([
                    'firstName' => $contact->getFirstName(),
                    'lastName' => $contact->getLastName(),
                    'title' => $contact->getSubject(),
                    'message' => $contact->getMessage(),
                ])
            ;
            $mailer->send($email);
            $this->addFlash(
                'success',
                'Votre mail a bien été envoyé'
            );
            return $this->redirectToRoute('home');
        }

        return $this->renderForm('contact/contact.html.twig', [
            'form' => $form,
        ]);

    }
}
