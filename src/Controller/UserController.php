<?php

namespace App\Controller;

use App\Entity\Painting;
use App\Entity\User;
use App\Form\EditProfileType;
use App\Repository\PaintingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    #[Route('/user', name: 'app_user')]
    public function profile(): Response
    {
        return $this->render('user/user.html.twig', [
            'controller_name' => 'UserController',
        ]);
    }
    #[Route('/user/edit-profile', name: 'app_edit-profile')]
    public function editProfile(Request $request, EntityManagerInterface $manager): Response
    {
        $user = $this->getUser();
        $form = $this->createForm(EditProfileType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $user->setUpdatedAt(new \DateTimeImmutable());
            $manager->flush();
            return $this->redirectToRoute('app_user');
        }
       return $this->renderForm('user/edit-profile.html.twig', [
           'form' => $form
       ]);
    }

    #[Route('/user/favorite/{id}', name: 'app_favorite')]
    public function favorite($id, PaintingRepository $paintingRepository, EntityManagerInterface $manager, Request $request): Response
    {
        $userEntity = $this->getUser();

        if ($userEntity == null){
            return $this->redirectToRoute('app_login');
        }
        $paint = $paintingRepository->find($id);
        $userEntity->addFavorite($paint);
        $manager->flush();
        $route = $request->headers->get('referer');

        return $this->redirect($route);
        //return $this->redirectToRoute('home');
    }


    #[Route('/user/cart', name: 'app_cart')]
    public function cart(): Response
    {
        return $this->render('user/cart.html.twig', [
        ]);
    }

    #[Route('/user/cart/remove/{id}', name: 'app_cart_remove')]
    public function removecart($id, PaintingRepository $paintingRepository, EntityManagerInterface $manager, Request $request): Response
    {
        $userEntity = $this->getUser();

        if ($userEntity == null){
            return $this->redirectToRoute('app_login');
        }
        $paint = $paintingRepository->find($id);
        $userEntity->removeCart($paint);
        $manager->flush();
        $route = $request->headers->get('referer');

        return $this->redirect($route);

    }


    #[Route('/user/cart/add/{id}', name: 'app_addCart')]
    public function addcart($id, PaintingRepository $paintingRepository, EntityManagerInterface $manager, Request $request): Response
    {
        $userEntity = $this->getUser();

        if ($userEntity == null){
            return $this->redirectToRoute('app_login');
        }
        $paint = $paintingRepository->find($id);
        $userEntity->addCart($paint);
        $manager->flush();
        $route = $request->headers->get('referer');

        return $this->redirect($route);
        //return $this->redirectToRoute('home');
    }
}
