<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Painting;
use App\Form\CommentType;
use App\Form\EditFormType;
use App\Form\NewPaintType;
use App\Form\RegistrationFormType;
use App\Repository\CommentRepository;
use App\Repository\PaintingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    #[Route('/admin', name: 'admin')]
    public function admin(PaintingRepository $paintingRepository): Response
    {
        $paintings = $paintingRepository->findby(
            [],
            ['createdAt' => 'DESC'],
        );
        return $this->render('admin/home.html.twig', [
            'paintings' =>  $paintings,
        ]);
    }

    #[Route('/admin/slider/{id}', name: 'app_admin_slider')]
    public function adminSlider(EntityManagerInterface $manager, Request $request, PaintingRepository $paintingRepository): Response
    {
        $routeParams = $request->attributes->get('_route_params');
        $painting = $paintingRepository->find($routeParams['id']);
        $painting->setIsSlider(!$painting->isIsSlider());

        $manager->flush();
        return $this->redirectToRoute('admin');

    }

    #[Route('/admin/edit/{id}', name: 'edit')]
    public function edit(EntityManagerInterface $manager, Painting $painting, Request $request): Response
    {
        $form = $this->createForm(EditFormType::class, $painting);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $manager->flush();
            return $this->redirectToRoute('admin');
        }
        return $this->renderForm('admin/edit.html.twig', [
            'form' => $form
        ]);
    }

    #[Route('/admin/delete/{id}', name: 'delete')]
    public function delete(EntityManagerInterface $manager, Request $request, PaintingRepository $paintingRepository): Response
    {
        $routeParams = $request->attributes->get('_route_params');
        $painting = $paintingRepository->find($routeParams['id']);

        $manager->remove($painting);
        $manager->flush();
        return $this->redirectToRoute('admin');
    }

    #[Route('/admin/newpaint', name: 'app_newpaint')]
    public function addPaint(EntityManagerInterface $entityManager, Request $request): Response
    {

        $paint = new Painting();
        $form = $this->createForm(NewPaintType::class, $paint);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $paint->setAddedAt(new \DateTimeImmutable());
            $entityManager->persist($paint);
            $entityManager->flush();

            $this->addFlash(
                'success',
                'Peinture ajoutée avec succès.'
            );
            return $this->redirectToRoute('home');
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    #[Route('/admin/{id}', name: 'adminPaint')]
    public function paint(Painting $paintings, EntityManagerInterface $manager, Request $request, PaintingRepository $paintingRepository, CommentRepository $commentRepository): Response
    {

        $routeParams = $request->attributes->get('_route_params');
        $painting = $paintingRepository->find($routeParams['id']);
        $comrep = $commentRepository->findby(
            ['painting' => $routeParams['id']],
            ['createdAt' => 'DESC']
        );

        $com = new Comment;
        $form = $this->createForm(CommentType::class, $com);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $com->setCreatedAt(new \DateTimeImmutable())
                ->setPainting($painting);
            $manager->persist($com);
            $manager->flush();
            return $this->redirect($routeParams['id']);
        }
        return $this->renderForm('admin/paint.html.twig', [
            'form' => $form,
            'paint' => $paintings,
            'coms' => $comrep

        ]);

    }


    #[Route('/admin/view/{paint}/{id}', name: 'view')]
    public function adminView(EntityManagerInterface $manager, Comment $comment, Request $request)
    {
        $routeParams = $request->attributes->get('_route_params');

        $comment->setIsPublished(!$comment->isIsPublished());
        $manager->flush();
        return $this->redirectToRoute('adminPaint', ['id' => $routeParams['paint']]);
    }


}
