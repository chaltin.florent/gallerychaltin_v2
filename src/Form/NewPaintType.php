<?php

namespace App\Form;

use App\Entity\Author;
use App\Entity\Category;
use App\Entity\Painting;
use App\Entity\Technical;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;


class NewPaintType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'label' => "Nom de l'oeurve:"
            ])
            ->add('author', EntityType::class, [
                'label' => 'Auteur',
                'class' => Author::class,
                'choice_label' => 'name',
                'placeholder' => "Auteur..."
            ])
            ->add('description', TextareaType::class, [
                'label' => "Description:"

            ])
            ->add('createdAt', DateType::class, [
                'label' => "Date de création de l'oeuvre:",
                'widget' => 'choice',
                'input'  => 'datetime_immutable',
                'years' => range(1,2000)


            ])
            ->add('height', NumberType::class, [
                'label' => "Hauteur:"

            ])
            ->add('width', NumberType::class, [
                'label' => "Longueur:"

            ])
            ->add('price', NumberType::class, [
                'label' => "Prix:"

            ])
            ->add('imageFile', VichFileType::class, [
                'required' => false,
                'allow_delete' => true,
                'delete_label' => '...',
                'download_uri' => '...',
                'download_label' => '...',
                'asset_helper' => true,
            ])
            ->add('category', EntityType::class, [
                'label' => 'Catégorie',
                'class' => Category::class,
                'choice_label' => 'name',
                'placeholder' => "Catérogie..."
            ])
            ->add('technical', EntityType::class, [
                'label' => 'Technique',
                'class' => Technical::class,
                'choice_label' => 'name',
                'placeholder' => "Technique..."
            ])
            ->add('isSlider', CheckboxType::class, [
                'label' => 'Dans le slider?',
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Painting::class,
        ]);
    }
}
